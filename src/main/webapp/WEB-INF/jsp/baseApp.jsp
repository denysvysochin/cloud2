<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<html>
<head>
    <title><tiles:getAsString name="title"/></title>
    <tiles:insertAttribute name="include"/>
</head>
<body>
  <tiles:insertAttribute name="menu"/>
  <tiles:insertAttribute name="body"/>
  <tiles:insertAttribute name="bottom"/>
</body>
</html>
