<div id="home-sec">
  <div class="container" style="margin-left: 20px">
  <div <%--class="row"--%>>
    <div class="col-md-1 col-md-offset-1">
      <div class="listContainer" style="width: 1580px;">

        <h2 data-bind="text: table().name"></h2>
        <div>
          <span><strong>Creation date: </strong></span>
          <span data-bind="text: table().dateCreation"></span>
        </div>

        <h3>Columns info : </h3>
        <div>
          <table border="2" align="center">
            <thead>
            <tr><th align="center">Name</th><th align="center">Type</th><th align="center">Size</th><th align="center">NOT NULL</th><th align="center">UNIQUE</th></tr>
            </thead>
            <tbody data-bind="foreach: columns">
            <tr>
              <td align="center" data-bind="text: name"></td>
              <td align="center" data-bind="text: dataType"></td>
              <td align="center" data-bind="text: sizeConstraint"></td>
              <td align="center"><input type="checkbox" data-bind="checked: notNullConstraint" disabled/></td>
              <td align="center"><input type="checkbox" data-bind="checked: uniqueConstraint" disabled/></td>
            </tr>
            </tbody>
          </table>
        </div>


        <h3>Data : </h3>

        <table border="2" align="center">
          <thead>
            <tr data-bind="foreach: columns">
              <th align="center" data-bind="text: name"></th>
            </tr>
          </thead>
          <tbody data-bind="foreach : sqlResult">
            <tr data-bind="foreach :$data">

              <td align="center">
                <!-- ko if: $index() ==  0 -->
                  <input type="text" disabled data-bind="value: d"/>
                <!-- /ko -->
                <!-- ko if: $index() != 0 -->
                  <input type="text" text-align="center" data-bind="value: d"/>
                <!-- /ko -->
              </td>
              <!-- ko if: $index() == ($parent.length - 1) -->
                <td align="center"><button class="btn-danger" data-bind="click: function () {$root.delete($parent, $parentContext.$index())}">X</button> </td>
              <!-- /ko -->
            </tr>

          </tbody>
        </table>
        <ul class="pagination" data-bind="foreach: paginationArray">
          <li <%--class="active"--%>><a href="#" data-bind="text: $data, click: function() {$root.getPage($data)}"></a></li>
        </ul>

        <div class="row" style="margin-top: 5px">
          <button class="btn btn-success" data-bind="click: addRow">Add row +</button>
          </div>
        <div class="row" style="margin-top: 5px">
          <button class="btn btn-primary" data-bind="click: save">Save</button>
        </div>

        <input type="hidden" id="tableId" value="${tableId}"/>
      </div>
    </div>
  </div>
  </div>
</div>

<script src="/resources/js/tableInfo.js"></script>