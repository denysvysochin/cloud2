<%--
<h1>Welcome to cloud DB</h1>--%>

<div id="home-sec">


    <div class="container" id="home" >
        <div class="row text-center g-pad-bottom">
            <div class="col-md-12">
                <span class="head-main">Cloud DB</span>
            </div>
        </div>
        <div class="row text-center">
            <div class="mainDescription">
                <div>Cloud DB its service for storage of your data</div>
                <div>Just create your database and us it via http requests</div>
            </div>

            <div>
                <img src="/resources/assets/img/cloud_main.png">
            </div>
            <%--<div class="col-md-4  col-sm-4">
                <i class="fa fa-desktop faa-vertical animated icon-round bk-color-red"></i>
                <h4>Sure Quique Menu </h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Curabitur nec nisl odio. Mauris vehicula at nunc id posuere.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

            </div>
            <div class="col-md-4  col-sm-4">
                <i class="fa fa-flask  faa-ring animated icon-round bk-color-green"></i>
                <h4>Sure Quique Menu </h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Curabitur nec nisl odio. Mauris vehicula at nunc id posuere.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

            </div>
            <div class="col-md-4  col-sm-4">
                <i class="fa fa-pencil  faa-shake animated icon-round bk-color-light-blue"></i>
                <h4>Sure Quique Menu </h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Curabitur nec nisl odio. Mauris vehicula at nunc id posuere.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

            </div>--%>
        </div>

    </div>
</div>

<section id="price-sec">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 text-center ">
                <div class="col-md-4 col-sm-4">
                    <ul class="plan">
                        <li class="plan-head btn-primary">TEST DB</li>

                        <li><strong>HDD</strong> Type</li>
                        <li><strong>5 MB</strong> Space</li>
                        <li><strong>Yes</strong> Support</li>

                        <li class="main-price">$0</li>
                        <li class="bottom">
                            <a href="/user/createDb" class="btn btn-danger btn-lg">Order</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4  col-sm-4">
                    <ul class="plan">
                        <li class="plan-head btn-success">EXTENDED DB</li>

                        <li><strong>HDD</strong> Type</li>
                        <li><strong>100 MB</strong> Space</li>
                        <li><strong>Yes</strong> Support</li>

                        <li class="main-price">$9.99</li>
                        <li class="bottom">
                            <a href="/order/2" class="btn btn-danger btn-lg">Order</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4  col-sm-4">
                    <ul class="plan">
                        <li class="plan-head btn-warning">PREMIUM DB</li>

                        <li><strong>HDD</strong> Type</li>
                        <li><strong>1 GB</strong> Space</li>
                        <li><strong>Yes</strong> Support</li>

                        <li class="main-price">$29.99</li>
                        <li class="bottom">
                            <a href="/order/3" class="btn btn-danger btn-lg">Order</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>




