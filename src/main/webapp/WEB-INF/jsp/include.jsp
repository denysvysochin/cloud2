
<script src="/resources/js/lib/knockout-3.4.1.js"></script>
<script src="/resources/js/lib/jquery-3.1.1.min.js"></script>
<script src="/resources/js/entities/UserDatabase.js"></script>
<script src="/resources/js/entities/MetaColumn.js"></script>
<script src="/resources/js/entities/MetaTable.js"></script>
<script src="/resources/js/entities/User.js"></script>

<link href="/resources/css/css.css" rel="stylesheet" />

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<title> Free Template- Simple Company</title>
<!--REQUIRED STYLE SHEETS-->
<!-- BOOTSTRAP CORE STYLE CSS -->
<link href="/resources/assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME STYLE CSS -->
<link href="/resources/assets/css/font-awesome.min.css" rel="stylesheet" />
<!--ANIMATED FONTAWESOME STYLE CSS -->
<link href="/resources/assets/css/font-awesome-animation.css" rel="stylesheet" />
<!-- CUSTOM STYLE CSS -->
<link href="/resources/assets/css/style.css" rel="stylesheet" />
<!-- GOOGLE FONT -->
<link href='/resources/css/fonts.css' rel='stylesheet' type='text/css'>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/resources/js/lib/html5shiv.js"></script>
<script src="/resources/js/lib/respond.min.js" type="text/javascript"></script>

<![endif]-->