<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="home-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-2">
                <div class="listContainer">
                    <h2>Your order:</h2>
                    <c:choose>
                        <c:when test="${pack eq 2}">
                            <div class="row">
                                <div class="col-md-2"><h4>Package: </h4></div>
                                <div class="col-md-2" style="background-color: #4cae4c; width: 130px"><h4>EXTENDED</h4></div>
                                <div class="col-md-6"></div>
                                <div class="col-md-1" style="font-weight: bold"><h4>9.99$</h4></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Includes: </div>
                            </div>
                            <div class="row">
                                <ul>
                                    <li><strong>HDD</strong> Type</li>
                                    <li><strong>100 MB</strong> Space</li>
                                    <li><strong>Yes</strong> Support</li>
                                </ul>
                            </div>
                            <hr style="border-top: 3px double #8c8b8b;">
                            <div class="row" style="margin-top: -80px">
                                <div class="col-md-9"><h1>Total:</h1></div>
                                <div class="col-md-3"><div class="col-md-2" style="font-weight: bold"><h1>9.99$</h1></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-11"></div>
                                <form name="tocheckout" method="POST" action="https://api.fondy.eu/api/checkout/redirect/">
                                    <input hidden type="text" name="order_id" value="${extendedOrderId}">
                                    <input hidden type="text" name="order_desc" value="Cloud DB. Package EXTENDED">
                                    <input hidden type="text" name="currency" value="USD">
                                    <input hidden type="text" name="amount" value="999">
                                    <input hidden type="text" name="signature" value="${extendedSignature}">
                                    <input hidden type="text" name="merchant_id" value="1397120">
                                    <div class="col-md-2"><button class="btn btn-success">Pay $</button></div>
                                </form>
                            </div>
                        </c:when>
                        <c:when test="${pack eq 3}">
                            <div class="row">
                                <div class="col-md-2"><h4>Package: </h4></div>
                                <div class="col-md-2" style="background-color: #eea236; width: 130px"><h4>PREMIUM</h4></div>
                                <div class="col-md-6"></div>
                                <div class="col-md-1" style="font-weight: bold"><h4>29.99$</h4></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Includes: </div>
                            </div>
                            <div class="row">
                                <ul>
                                    <li><strong>HDD</strong> Type</li>
                                    <li><strong>1 GB</strong> Space</li>
                                    <li><strong>Yes</strong> Support</li>
                                </ul>
                            </div>
                            <hr style="border-top: 3px double #8c8b8b;">
                            <div class="row" style="margin-top: -80px">
                                <div class="col-md-9"><h1>Total:</h1></div>
                                <div class="col-md-3"><div class="col-md-2" style="font-weight: bold"><h1>29.99$</h1></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-11"></div>
                                <form name="tocheckout" method="POST" action="https://api.fondy.eu/api/checkout/redirect/">
                                    <input hidden type="text" name="order_id" value="${premiumOrderId}">
                                    <input hidden type="text" name="order_desc" value="Cloud DB. Package PREMIUM">
                                    <input hidden type="text" name="currency" value="USD">
                                    <input hidden type="text" name="amount" value="2999">
                                    <input hidden type="text" name="signature" value="${premiumSignature}">
                                    <input hidden type="text" name="merchant_id" value="1397120">
                                    <div class="col-md-2"><button class="btn btn-success">Pay $</button></div>
                                </form>
                            </div>
                        </c:when>
                    </c:choose>



                </div>
            </div>
        </div>
    </div>
</div>