<div id="home-sec">
  <div class="container">
  <div class="row">
    <div class="col-md-2 col-md-offset-2">
      <div class="listContainer">

        <h2 align="center">Documentation</h2>

        <h3>
          About Cloud DB
        </h3>

        <div class="documentation">
          Cloud DB its service for storage data into database based on Oracle DB.
          Database can be created via web application, and data can be modified and retrieved via web API.
        </div>

        <h3>
          Database
        </h3>

        <div class="documentation">
          Database its main entity in Cloud DB system, which contains tables.
          Tables have 3 types : TEST, EXTENDED, PREMIUM.
          Firs of all you have to login into system. Then open "MY DB`s" link and press "Create"
        </div>

        <h3>
          Table
        </h3>

        <div class="documentation">
          Table its entity that represents Oracle table.
          It can be created into Database info page by pressed "Create" button.
        </div>

        <h3>
          API
        </h3>

        <div class="documentation">
          For running sql queries onto your table, you have to use api call.
          <div>Url template : www.cloud-db.com/{dbName}/{secreteKey}?query={sql query}</div>
          <ul>
            <li><strong>dbName : </strong> Name of your Database when you want to run query</li>
            <li><strong>secreteKey : </strong>Your secrete key which you can know into "Profile"</li>
            <li><strong>sql query : </strong>Sql query</li>
          </ul>
        </div>

      </div>
    </div>
  </div>
  </div>
</div>
