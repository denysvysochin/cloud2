<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--<div>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/user/dbList">Db List</a></li>
  </ul>
</div>--%>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <%--<a class="navbar-brand" href="#">YOUR LOGO</a>--%>
      <img src="/resources/assets/img/cloud.png" width="70" height="50">
      <span class="navbar-brand">Cloud DB</span>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/">HOME</a></li>
        <li><a href="/documentation">DOCUMENTATION</a></li>
        <sec:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN">
          <li><a href="/user/dbList">MY DB`s</a></li>
          <li><a href="/user/support">SUPPORT</a></li>
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_ADMIN">
          <li><a href="/admin">ADMIN PANEL</a></li>
        </sec:authorize>
        <li><a>|</a></li>
        <sec:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN">
          <li><a href="/user/profile">PROFILE</a></li>
          <li><a href="/logout">LogOut</a></li>
        </sec:authorize>
        <sec:authorize ifNotGranted="ROLE_USER, ROLE_ADMIN">
          <li><a href="/login">LogIn</a></li>
          <li><a href="/registration">Registration</a></li>
        </sec:authorize>

      </ul>
    </div>

  </div>
</div>
