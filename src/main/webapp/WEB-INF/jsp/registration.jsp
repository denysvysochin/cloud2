<div id="home-sec">

<%--<h2>Registration : </h2>
<div>
  <div>
    <div>
      <span>Email : </span>
    </div>
    <div>
      <input type="email" data-bind="value : email">
    </div>
    <div>
      <span>Name : </span>
    </div>
    <div>
      <input type="text" data-bind="value : name">
    </div>
    <div>
      <span>Second name: </span>
    </div>
    <div>
      <input type="text" data-bind="value : secondName">
    </div>
    <div>
      <span>Password : </span>
    </div>
    <div>
      <input type="password" data-bind="value : password">
    </div>
    <div>
      <span>Repeat password : </span>
    </div>
    <div>
      <input type="password" data-bind="value : repeatPassword">
    </div>
  </div>
  <div>
    <span data-bind="text: errorMessage"></span>
  </div>
  <button data-bind="click: registrate" >Registration</button>
</div>--%>

  <div class="row">
    <div class="col-md-2 col-md-offset-3">
<div class="registrationContainer1 mainRegContainer">
  <div class="row main">
    <div class="panel-heading">
      <div class="panel-title text-center">
        <h1 class="title">Registration</h1>
        <hr />
      </div>
    </div>
    <div class="errorMessage">
      <span data-bind="text: errorMessage"></span>
    </div>
    <div class="main-login main-center registrationContainer">
      <form class="form-horizontal" method="post" action="#">

        <div class="form-group">
          <label for="email" class="cols-sm-2 control-label">Your Email</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <input type="email" data-bind="value : email" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Your Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" data-bind="value : name" class="form-control" name="name" id="name"  placeholder="Enter your Name"/>
            </div>
          </div>
        </div>



        <div class="form-group">
          <label for="username" class="cols-sm-2 control-label">Your Second Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
              <input type="text" data-bind="value : secondName" class="form-control" name="username" id="username"  placeholder="Your Second Name"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password"  data-bind="value : password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" data-bind="value : repeatPassword" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
            </div>
          </div>
        </div>

        <%--<div class="form-group">
          <div class="cols-sm-10">
          <div class="errorMessage">
            <span data-bind="text: errorMessage"></span>
          </div>
            </div>
        </div>--%>

        <div class="form-group ">
          <button type="button" data-bind="click: registrate" class="btn btn-primary btn-lg btn-block login-button">Register</button>
        </div>
        <%--<div class="login-register">
          <a href="index.php">Login</a>
        </div>--%>
      </form>
    </div>
  </div>
</div>
    </div>
  </div>

<script type="text/javascript" src="/resources/assets/js/bootstrap.js"></script>

<script src="/resources/js/registration.js"></script>
</div>