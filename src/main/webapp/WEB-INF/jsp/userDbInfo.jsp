<div id="home-sec">
  <div class="container">
  <div class="row">
    <div class="col-md-2 col-md-offset-2">
      <div class="listContainer">
        <h2 data-bind="text: dbName"></h2>

        <div>
          <span>Type : </span><span data-bind="text: type"></span>
          <span>Date creation : </span><span data-bind="text: date"></span>
        </div>
        <hr>
        <h3>Tables : </h3>

        <ol data-bind="foreach: tables">
          <li>
            <a class="listMember" data-bind="attr: { href: '/user/tableInfo/' + id}">
              <span data-bind="text: name"></span></a>
            <span><strong>Creation date: </strong></span>
            <span data-bind="text: dateCreation"></span>
          </li>
        </ol>

        <a class="btn btn-primary" href="/user/createTable/${dbId}">Create table +</a>

        <input type="hidden" id="dbId" value="${dbId}"/>
      </div>
    </div>
  </div>
  </div>
</div>
<script src="/resources/js/userDbInfo.js"></script>
