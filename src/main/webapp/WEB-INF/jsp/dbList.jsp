<div id="home-sec">
    <div class="container">
    <div class="row">
        <div class="col-md-2 col-md-offset-2">
            <div class="listContainer">
                <h2>My databases</h2>

                <div class="descriptionInfo">
                    This is your databases
                </div>

                <hr>


                <ol data-bind="foreach: dbList">
                    <li>
                        <a class="listMember" data-bind="attr: { href: '/user/userDbInfo/' + id}">
                        <span data-bind="text: dbName"></span>
                    </a>
                        <span><strong>Type: </strong></span>
                        <span data-bind="text: type"></span>
                        <span><strong>Creation date: </strong></span>
                        <span data-bind="text: date"></span></li>
                        <canvas id="some-simple-line-chart"
                            data-bind="chart: { type: 'pie', data: pieData }"></canvas>
                </ol>

                <a class="btn btn-primary" href="/user/createDb">Create new +</a>

            </div>
        </div>
    </div>
        </div>
</div>
<script src="/resources/js/lib/chart.js" type="text/javascript"></script>
<script src="/resources/js/lib/knockout.chart.js" type="text/javascript"></script>
<script src="/resources/js/dbList.js"></script>
