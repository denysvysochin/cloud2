<div id="home-sec">
  <div class="container">
  <div class="row">
    <div class="col-md-2 col-md-offset-2">
      <div class="listContainer">
        <h2>Create table</h2>

        <h3>Table : </h3>


        <div class="form-group">
          <label>Name : </label>
          <input type="text" data-bind="value: tableName" class="form-control">
        </div>



        <h3>Columns : </h3>

        <ol data-bind="foreach: columns">
          <li>
            <div class="form-group">
              <label>Name : </label>
              <input type="text" data-bind="value: name" class="form-control">
            </div>

            <div class="form-group">
              <label>Data type : </label>
              <select class="form-control" data-bind="value: dataType">
                <option>DECIMAL</option>
                <option>VARCHAR</option>
                <option>TEXT</option>
              </select>
            </div>

            <div class="form-group">
              <label>Size constraint : </label>
              <input class="form-control" type="number" data-bind="value: sizeConstraint"/>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6">
                    <label>NOT NULL : </label>
                  </div>
                  <div class="col-md-6">
                    <input class="form-control" type="checkbox" data-bind="checked: notNullConstraint"/>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6">
                    <label>UNIQUE : </label>
                  </div>
                  <div class="col-md-6">
                    <input class="form-control" type="checkbox" data-bind="checked: uniqueConstraint"/>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn-danger" data-bind="click: function() {$root.deleteColumn($index)}">X</button>
<%--
            <div class="form-group">



            </div>--%>

          </li>
          <hr>
        </ol>

        <div class="tableButton">
          <button class="btn btn-success" data-bind="click: addColumn">Add column +</button>
        </div>
        <div data-bind="visible: columns().length > 0">
          <button class="btn btn-primary" data-bind="click: create">Create</button>
        </div>

        <input type="hidden" id="dbId" value="${dbId}"/>
      </div>
    </div>
  </div>
  </div>
</div>
<script src="/resources/js/createTable.js"></script>