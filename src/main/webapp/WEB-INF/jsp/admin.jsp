<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="home-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-2">
                <div class="listContainer" style="width: 1200px; margin-left: -240px">
                    <h2 align="center">Dashboard</h2>
                    <div class="row" style="background-color: #EBE8ED; border-radius: 12px;box-shadow: 10px 10px grey;">
                        <div class="col-md-4" style="padding: 20px;"><img src="/resources/img/2017-12-02_17h30_04.png" width="50"><span style="font-size: 20px; padding-left: 10px">Users :</span> <span style="font-size: 20px">${usersCount}</span></div>
                        <div class="col-md-4" style="padding: 20px;"><img src="/resources/img/2017-12-02_17h30_24.png" width="50"><span style="font-size: 20px; padding-left: 10px">Visits :</span>  <span style="font-size: 20px">${visits}</span></div>
                        <div class="col-md-4" style="padding: 20px;"><img src="/resources/img/2017-12-02_17h30_42.png" width="50"><span style="font-size: 20px; padding-left: 10px">Orders :</span>  <span style="font-size: 20px">${orders}</span></div>
                    </div>

                    <div class="row" style="margin-top: 20px">
                        <div id="chartContainer3" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                    </div>
                    <div class="row">
                        <div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                    </div>
                    <div class="row">
                        <div id="chartContainer1" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                    </div>

                    <div class="row">
                        <div id="chartContainer2" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                    </div>

                    <div class="row">
                        <h2>Users</h2>
                        <ul class="list-group">
                        <c:forEach items="${users}" var="user">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-2" style="font-weight: bold">${user.email}</div>
                                    <div class="col-md-1" style="font-weight: bold">${user.name}</div>
                                    <div class="col-md-2" style="font-weight: bold">${user.secondName}</div>
                                    <div class="col-md-1" style="color: dodgerblue">TEST: ${user.testCount}</div>
                                    <div class="col-md-1" style="color: green">Extended:${user.extendetCount}</div>
                                    <div class="col-md-2" style="color: #eea236">Premium:${user.premiumCount}</div>
                                    <div class="col-md-1" style="font-style: italic; font-weight: bold">${user.userRole}</div>
                                </div>
                            </li>
                        </ul>
                        </c:forEach>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {

        var chart3 = new CanvasJS.Chart("chartContainer3", {
            animationEnabled: true,
            exportEnabled: true,
            title:{
                text: "Revenue"
            },
            axisY:{
                title: "Percentage",
                includeZero: false,
                interval: .2,
                prefix: "$"
            },
            data: [{
                type: "line",
                yValueFormatString: "#0.0'%'",
                xValueFormatString: "MMM YYYY",
                markerSize: 5,
                dataPoints: [
                    { x: new Date(2016, 0), y: 4.9 },
                    { x: new Date(2016, 1), y: 4.9 },
                    { x: new Date(2016, 2), y: 5.0 },
                    { x: new Date(2016, 3), y: 5.0, indexLabel: "Highest", indexLabelFontColor: "#C24642" },
                    { x: new Date(2016, 4), y: 4.7 },
                    { x: new Date(2016, 5), y: 4.9 },
                    { x: new Date(2016, 6), y: 4.9 },
                    { x: new Date(2016, 7), y: 4.9 },
                    { x: new Date(2016, 8), y: 4.9 },
                    { x: new Date(2016, 9), y: 4.8 },
                    { x: new Date(2016, 10), y: 4.6 },
                    { x: new Date(2016, 11), y: 4.7 },
                    { x: new Date(2017, 0), y: 4.8 },
                    { x: new Date(2017, 1), y: 4.7 },
                    { x: new Date(2017, 2), y: 4.5 },
                    { x: new Date(2017, 3), y:4.4 },
                    { x: new Date(2017, 4), y:4.3 },
                    { x: new Date(2017, 5), y:4.4 }
                ]
            }]
        });
        chart3.render();


        var chart1 = new CanvasJS.Chart("chartContainer1", {
            exportEnabled: true,
            animationEnabled: true,
            title:{
                text: "Memory Usage HDD"
            },
            legend:{
                cursor: "pointer",
                itemclick: explodePie
            },
            data: [{
                type: "pie",
                showInLegend: true,
                toolTipContent: "{name}: <strong>{y}MB</strong>",
                indexLabel: "{name} - {y}MB",
                dataPoints: [
                    { y: '${free}', name: "Free", exploded: true },
                    { y: '${usable}', name: "Usage" }
                ]
            }]
        });
        chart1.render();
        var chart2 = new CanvasJS.Chart("chartContainer2", {
            exportEnabled: true,
            animationEnabled: true,
            title:{
                text: "Memory Usage SSD"
            },
            legend:{
                cursor: "pointer",
                itemclick: explodePie
            },
            data: [{
                type: "pie",
                showInLegend: true,
                toolTipContent: "{name}: <strong>{y}MB</strong>",
                indexLabel: "{name} - {y}MB",
                dataPoints: [
                    { y: 84131840, name: "Free", exploded: true },
                    { y: 41943040, name: "Usage" }

                ]
            }]
        });
        chart2.render();

    function explodePie (e) {
        if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
            e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
        } else {
            e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
        }
        e.chart.render();

    }

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title:{
                text: "Package orders"
            },
            axisY :{
                includeZero: false
                /*prefix: "$"*/
            },
            toolTip: {
                shared: true
            },
            legend: {
                fontSize: 13
            },
            data: [{
                type: "splineArea",
                showInLegend: true,
                name: "Test",
                dataPoints: [
                    { x: new Date(2016, 2), y: 2 },
                    { x: new Date(2016, 3), y: 3 },
                    { x: new Date(2016, 4), y: 5 },
                    { x: new Date(2016, 5), y: 4 },
                    { x: new Date(2016, 6), y: 6 },
                    { x: new Date(2016, 7), y: 6 },
                    { x: new Date(2016, 8), y: 3 },
                    { x: new Date(2016, 9), y: 5 },
                    { x: new Date(2016, 10), y: 7},
                    { x: new Date(2016, 11), y: 8 },
                    { x: new Date(2017, 0),  y: 8 },
                    { x: new Date(2017, 1),  y: 7 }
                ]
            },
                {
                    type: "splineArea",
                    showInLegend: true,
                    name: "Extendet",
                    dataPoints: [
                        { x: new Date(2016, 2), y: 1 },
                        { x: new Date(2016, 3), y: 1 },
                        { x: new Date(2016, 4), y: 2 },
                        { x: new Date(2016, 5), y: 3 },
                        { x: new Date(2016, 6), y: 2 },
                        { x: new Date(2016, 7), y: 4 },
                        { x: new Date(2016, 8), y: 5 },
                        { x: new Date(2016, 9), y: 3 },
                        { x: new Date(2016, 10), y: 4 },
                        { x: new Date(2016, 11), y: 5 },
                        { x: new Date(2017, 0), y: 5 },
                        { x: new Date(2017, 1), y: 1 }
                    ]
                },
                {
                    type: "splineArea",
                    showInLegend: true,
                    name: "Premium",
                    dataPoints: [
                        { x: new Date(2016, 2), y: 2 },
                        { x: new Date(2016, 3), y: 1 },
                        { x: new Date(2016, 4), y: 3 },
                        { x: new Date(2016, 5), y: 1 },
                        { x: new Date(2016, 6), y: 1 },
                        { x: new Date(2016, 7), y: 2 },
                        { x: new Date(2016, 8), y: 1 },
                        { x: new Date(2016, 9), y: 3 },
                        { x: new Date(2016, 10), y: 4 },
                        { x: new Date(2016, 11), y: 2 },
                        { x: new Date(2017, 0), y: 2 },
                        { x: new Date(2017, 1), y: 1 }
                    ]
                }]
        });
        chart.render();

    }
</script>
<script src="/resources/js/canvasjs.min.js"></script>