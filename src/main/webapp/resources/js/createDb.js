/**
 * Created by admin on 29.11.2016.
 */

var createDbModel = function(){
    var self = this;
    self.name = ko.observable("");
    self.type = ko.observable("Default");

    self.send = function() {
        var newDb = {
            dbName: self.name(),
            type: self.type()
        };
        $.post("/user/db/create", newDb, function(returnedData) {
            try {
                if (JSON.parse(returnedData).errorMessage) {
                    alert(JSON.parse(returnedData).errorMessage)
                } else {
                    window.location.replace(returnedData);
                }
            }catch (e){}
        });
    };

    var init = function() {

        /*$.get("/db/get", null, function(returnedData) {
            console.log(returnedData);
        });*/
    };



    init();
};

ko.applyBindings(new createDbModel());