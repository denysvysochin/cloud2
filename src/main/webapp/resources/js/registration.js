/**
 * Created by admin on 03.12.2016.
 */

/**
 * Created by admin on 30.11.2016.
 */

var registrationModel = function(){
    var self = this;

    self.email = ko.observable("");
    self.name = ko.observable("");
    self.secondName= ko.observable("");
    self.password = ko.observable("");
    self.repeatPassword = ko.observable("");

    self.errorMessage = ko.observable("");


    self.registrate = function() {


        if(validate()) {
            var user = {
                email : self.email(),
                name : self.name(),
                secondName : self.secondName(),
                password : self.password()

            };

            self.send(user);
        }
    };

    self.send = function(user) {

        $.post("/registration/registrate", user, function(returnedData) {
            window.location.replace(returnedData);
        });
    };



    var validate = function() {

        var isAllFilled = !((self.email() == undefined || self.email() == "" )
            && (self.name() == undefined || self.name() == "")
            && (self.secondName()  == undefined || self.secondName()  == "")
            && (self.password() == undefined || self.password() == "")
            && (self.repeatPassword() == undefined || self.repeatPassword() == ""));
        if(!isAllFilled) {
            self.errorMessage("All fields are required !");
            return false;
        } else if (!validateEmail()) {
            self.errorMessage("Email is already used");
            return false;
        } else if (self.password() != self.repeatPassword()) {
            self.errorMessage("Passwords are not the same !");
            return false;
        } else {
            return true;
        }
    };

    var validateEmail = function () {
        var email = {
            email : self.email()
        };
        var isValid = false;

        $.ajax({
            type: "GET",
            url: "/registration/validateEmail",
            data: email,
            async: false,
            contentType: "application/json",
            complete: function (returnedData) {
                isValid = (returnedData.responseText == "true");
            }});

        return isValid;
    };


    var init = function() {};

    init();
};

ko.applyBindings(new registrationModel());
