/**
 * Created by admin on 01.12.2016.
 */


var tableInfo = function(){
    var self = this;
    self.unsavedCount = 0;
    self.isEmpty = true;
    self.allRowsCount = 0;
    self.paginationArray = ko.observableArray([]);
    self.rowsPerPage = 7;
    self.currentPage = 1;

    self.table = ko.observable(new MetaTable("","","",""));
    self.columns = ko.observableArray([]);

    self.sqlResult = ko.observableArray([]);

    self.addRow = function () {
        self.isEmpty = false;
        self.unsavedCount++;
        var newRow = [];
        for (var i = 0; i < self.columns().length; i++) {
            newRow.push("");
        }

        self.sqlResult.push(ko.observableArray(
            ko.utils.arrayMap(newRow, function(row) {
                return ko.observable({d: ko.observable(row)});
            })));
    };

    self.save = function () {
        self.sqlResult().forEach(function(item) {
            if(item()[0]().d()) {
                var sql = "UPDATE " + self.table().name +  " SET ";
                item().forEach(function (row, i) {
                    if (i != 0) {
                        sql += " , ";
                    }
                    sql += self.columns()[i].name + " = ";
                    if (self.columns()[i].dataType == "INT" || self.columns()[i].dataType == "DECIMAL") {
                        sql += row().d() + " ";
                    } else {
                        sql += "'" + row().d() + "' ";
                    }
                });
                sql += "WHERE RowNumber = " + item()[0]().d();
            } else {
                var sql = "INSERT INTO " + self.table().name +  " (";
                    self.columns().forEach(function (c, i) {
                        if (i != 0) {
                            if (i != 1) {
                                sql += ",";
                            }
                            sql += c.name;
                        }
                    });
                    sql += ") VALUES (";
                    item().forEach(function (row, i) {
                        if (i != 0) {
                            if (i != 1) {
                                sql += ",";
                            }
                            if (self.columns()[i].dataType == "INT" || self.columns()[i].dataType == "DECIMAL") {
                                sql += row().d();
                            } else {
                                sql += "'" + row().d() + "'";
                            }
                        }
                    });
                    sql += ");";
            }
            var query = {
                query : sql
            };
            runQuery(query)
        });
    };

    self.delete = function (data, index) {
        if (confirm("Delete this record") == true) {
            var id = data[0]().d();
            if (id) {
                var sql = "DELETE FROM " + self.table().name +  " WHERE RowNumber = ";
                sql += id;
                var query = {
                    query : sql
                };
                self.unsavedCount = 0;
                /**/
                runQuery(query)
            } else {
                self.sqlResult.splice(index,1);
                //console.log(1);
            }

        }


    };

    self.getPage = function (page) {
        self.currentPage = page;
        var query = {
            //query : "SELECT * FROM " + self.table().name +  ""
            query : "SELECT * FROM " + self.table().name + " LIMIT " + (self.currentPage*self.rowsPerPage-self.rowsPerPage) + "," + self.rowsPerPage
        };
        self.sqlResult.splice(0,self.sqlResult().length);
        runQuery(query);
    };

    var init = function() {
        $.get("/user/table/" + $("#tableId").val(), null, function (returnedData) {
            if (returnedData != undefined) {
                self.table(new MetaTable(returnedData.id, returnedData.name, returnedData.dateCreation, returnedData.dbId, returnedData.dbName, returnedData.userKey));
                returnedData.columns.forEach(function(item) {
                    self.columns.push(new MetaColumn(item.id, item.name, item.dataType, item.sizeConstraint, item.notNullConstraint, item.uniqueConstraint))

                });

                var query = {
                    query : "SELECT COUNT(*) FROM " + self.table().name +  ""
                };
                $.ajax({
                    type: "GET",
                    url: "/query/run/" + self.table().dbName + "/" + self.table().userKey,
                    data: query,
                    contentType: "application/json",
                    complete: function (returnedData) {
                        self.allRowsCount = returnedData.responseText.substring(1,returnedData.responseText.length-1)/self.rowsPerPage;
                        if (returnedData.responseText.substring(1,returnedData.responseText.length-1)%self.rowsPerPage != 0) {
                            self.allRowsCount++;
                        }
                        for (var i = 1; i<=self.allRowsCount; i++) {
                            self.paginationArray.push(i);
                        }
                    }
                });
                query = {
                    //query : "SELECT * FROM " + self.table().name +  ""
                    query : "SELECT * FROM " + self.table().name + " LIMIT " + (self.currentPage*self.rowsPerPage-self.rowsPerPage) + "," + self.rowsPerPage
                };
                runQuery(query);
            }
        });


    };

    var runQuery = function (query) {
        $.ajax({
            type: "GET",
            url: "/query/run/"+self.table().dbName+"/"+self.table().userKey,
            data: query,
            contentType: "application/json",
            complete: function (returnedData) {
                try {
                    var response = JSON.parse(returnedData.responseText);
                    if (response.errorMessage) {
                        alert(response.errorMessage);
                        return;
                    }
                } catch (e){}
                var arr = $.parseJSON(returnedData.responseText)
                if (arr.length == 0) {
                    self.unsavedCount--;
                    if (self.unsavedCount < 1 && !self.isEmpty) {
                        alert("Data saved successful!");
                        self.sqlResult.splice(0,self.sqlResult().length);
                        //self.columns.splice(0,self.sqlResult().length);
                        var query = {
                            query : "SELECT * FROM " + self.table().name + " LIMIT " + (self.currentPage*self.rowsPerPage-self.rowsPerPage) + "," + self.rowsPerPage
                        };
                        if (self.sqlResult().length == 0) {
                            self.isEmpty = true;
                        }

                        runQuery(query);
                        //init();
                    }
                    return;
                }

                arr.forEach(function (data) {
                    self.sqlResult.push(ko.observableArray(ko.utils.arrayMap(data, function(row) {
                        return ko.observable({d: ko.observable(row)});
                    })));
                });
                self.unsavedCount = self.sqlResult().length;
                if (self.unsavedCount != 0) {
                    self.isEmpty = false;
                }
            }});
    };

    init();
};

ko.applyBindings(new tableInfo());