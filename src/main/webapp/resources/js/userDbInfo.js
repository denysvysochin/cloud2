/**
 * Created by admin on 30.11.2016.
 */

var dbInfo = function(){
    var self = this;

    self.dbName = ko.observable();
    self.type = ko.observable();
    self.date = ko.observable();
    self.tables = ko.observableArray();

    var init = function() {

        $.get("/user/db/info/"+$("#dbId").val(), null, function(returnedData) {
            if (returnedData != undefined) {
                self.dbName(returnedData.dbName);
                self.type(returnedData.type);
                self.date(returnedData.dateCreation);
            }
        });

        $.get("/user/table/list/"+$("#dbId").val(), null, function(returnedData) {
            if (returnedData != undefined) {
                returnedData.forEach(function(item) {
                    self.tables.push(new MetaTable(item.id, item.name, item.dateCreation, item.dbId));
                });
            }
        });
    };

    init();
};

ko.applyBindings(new dbInfo());