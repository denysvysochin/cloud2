/**
 * Created by admin on 29.11.2016.
 */

var dbList = function(){
    var self = this;

    self.dbList = ko.observableArray();

    var init = function() {

        $.get("/user/db/list", null, function(returnedData) {
            if (returnedData != undefined) {
                returnedData.forEach(function(item, index) {
                    var SimpleLineData = {
                        labels: ["Used", "Left"],
                        datasets: [
                            {
                                data: [item.spaceLeft, 5-item.spaceLeft],
                                backgroundColor: [
                                    "#FF6384",
                                    "#36A2EB"
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB"
                                ]
                            }
                        ]
                    };



                    self.dbList.push(new UserDatabase(item.id, item.dbName, item.type, item.dateCreation, SimpleLineData));
                })
            }
        });
    };

    init();
};

ko.applyBindings(new dbList());
