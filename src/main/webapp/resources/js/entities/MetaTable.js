/**
 * Created by admin on 30.11.2016.
 */

function MetaTable(id, name, dateCreation, dbId, dbName, userKey) {
    this.id = id;
    this.name = name;
    this.dateCreation = dateCreation;
    this.dbId = dbId;
    this.dbName = dbName;
    this.userKey = userKey;
}