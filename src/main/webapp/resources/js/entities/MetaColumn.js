/**
 * Created by admin on 30.11.2016.
 */

function MetaColumn(id, name, dataType, sizeConstraint, notNullConstraint, uniqueConstraint) {
    this.id = id;
    this.name = name;
    this.dataType = dataType;
    this.sizeConstraint = sizeConstraint;
    this.notNullConstraint = notNullConstraint;
    this.uniqueConstraint = uniqueConstraint;

}