/**
 * Created by admin on 04.12.2016.
 */

var profileModel = function(){
    var self = this;

    //self.user = ko.observable();
    self.email = ko.observable("");
    self.name = ko.observable("");
    self.secondName = ko.observable("");
    self.userKey = ko.observable("");


    var init = function() {

        $.get("/user/profile/info", null, function(returnedData) {
            if (returnedData != undefined) {
                self.email(returnedData.email);
                self.name(returnedData.name);
                self.secondName(returnedData.secondName);
                self.userKey(returnedData.userKey);
            }
        });
    };

    init();
};

ko.applyBindings(new profileModel());