/**
 * Created by admin on 30.11.2016.
 */

var createTableModel = function(){
    var self = this;

    self.tableName = ko.observable("");
    self.columns = ko.observableArray();

    self.addColumn = function() {
        self.columns.push(new MetaColumn(0, "", "DECIMAL", undefined, false, false));
    };

    self.deleteColumn = function (i) {
        self.columns.splice(i(),1);
    };

    self.create = function() {

        var newTable = {
            name: self.tableName(),
            dbId: $("#dbId").val(),
            columns: self.columns()
        };

        //newTable = newTable.toJSON;
        //console.log();
        self.send(JSON.stringify(newTable))
    };

    self.send = function(data) {
        $.ajax({
            type: "POST",
            url: "/user/table/create",
            data: data,
            contentType: "application/json",
            complete: function (returnedData) {
                window.location.replace(returnedData.responseText);
        }});
        /*$.post("/table/create", data, function(returnedData) {
            window.location.replace(returnedData);
        },"application/json;odata=verbose");*/
    };


    var init = function() {};

    init();
};

ko.applyBindings(new createTableModel());
