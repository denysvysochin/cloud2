create table USER_TABLE (
  user_id INT(10) NOT NULL UNIQUE AUTO_INCREMENT,
  user_name varchar(50),
  second_name varchar(50),
  email varchar(50) NOT NULL,
  password varchar(50) NOT NULL
);

create table USER_DB (
  db_id INT(10) NOT NULL UNIQUE AUTO_INCREMENT,
  user_id INT(10),
  db_name varchar(50),
  db_type varchar(50),
  date_creation varchar(50)
);

 CREATE TABLE META_TABLE (
  table_id INT(10) NOT NULL UNIQUE AUTO_INCREMENT,
  db_id INT(10),
  table_name varchar(50),
  date_creation varchar(50)
 );

 CREATE TABLE META_COLUMN (
  column_id INT(10) NOT NULL UNIQUE AUTO_INCREMENT,
  table_id INT(10),
  column_name varchar(50),
  data_type INT(10),
  size_constraint INT(10),
  not_null_constraint INT(1),
  unique_constraint INT(1)
 );

alter table user_table add user_role varchar(20);

ALTER TABLE USER_TABLE ADD user_key varchar(50);


CREATE TABLE VISITS (
  id INT(10) NOT NULL UNIQUE AUTO_INCREMENT,
  numbers INT(10)
);

INSERT INTO VISITS (numbers) VALUES (0);