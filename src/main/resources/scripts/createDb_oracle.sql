create table USER_TABLE (
  user_id NUMBER(10) NOT NULL UNIQUE,
  user_name VARCHAR2(50),
  second_name VARCHAR2(50),
  email VARCHAR2(50) NOT NULL,
  password VARCHAR2(50) NOT NULL
);

create table USER_DB (
  db_id NUMBER(10) NOT NULL UNIQUE,
  user_id NUMBER(10),
  db_name VARCHAR2(50),
  db_type VARCHAR2(50),
  date_creation VARCHAR2(50)
);

CREATE SEQUENCE user_db_ids
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 CREATE SEQUENCE user_ids
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 CREATE SEQUENCE meta_table_ids
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 CREATE SEQUENCE meta_column_ids
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 CREATE TABLE META_TABLE (
  table_id NUMBER(10) NOT NULL UNIQUE,
  db_id NUMBER(10),
  table_name VARCHAR2(50),
  date_creation VARCHAR2(50)
 );

 CREATE TABLE META_COLUMN (
  column_id NUMBER(10) NOT NULL UNIQUE,
  table_id NUMBER(10),
  column_name VARCHAR2(50),
  data_type NUMBER(10),
  size_constraint NUMBER(10),
  not_null_constraint NUMBER(1),
  unique_constraint NUMBER(1)
 );

alter table user_table add user_role VARCHAR2(20);

ALTER TABLE USER_TABLE ADD user_key varchar(50);