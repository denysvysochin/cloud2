package com.den.cloud.services;

import com.den.cloud.dao.UserDao;
import com.den.cloud.dto.UserDto;
import com.den.cloud.entities.User;
import com.den.cloud.enums.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by Denys Vysochin on 29.11.2016.
 */

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public User getUserEntityByEmail(String email) {
        return userDao.getUserEntityByEmail(email);
    }

    public UserDto getUser(int id) {
        return userDao.getUser(id);
    }

    public void createUser(UserDto userDto) {
        userDto.setUserRole(UserRole.ROLE_USER);
        /*MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update((LocalDateTime.now().toString() + userDto.getName()).getBytes());*/
        String userKey = "";
        for (int i = 1; i<4; i++) {
            userKey += (LocalDateTime.now().toString() + userDto.getName()).getBytes().toString().substring(2).substring(i);
        }

        userDto.setUserKey(userKey);
        userDao.createUser(userDto);
    }

    public boolean emailIsExist(String email) {
        return !(getUserEntityByEmail(email) == null);
    }

}
