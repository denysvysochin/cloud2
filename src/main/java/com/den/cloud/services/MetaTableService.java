package com.den.cloud.services;

import com.den.cloud.cloudengine.CodeGenerator;
import com.den.cloud.dao.CloudDBEngineDao;
import com.den.cloud.dao.MetaTableColumnDao;
import com.den.cloud.dto.MetaColumnDto;
import com.den.cloud.dto.MetaTableDto;
import com.den.cloud.dto.UserDto;
import com.den.cloud.enums.DataTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */

@Service
public class MetaTableService {

    @Autowired
    private MetaTableColumnDao metaTableColumnDao;

    @Autowired
    private CodeGenerator codeGenerator;

    @Autowired
    private CloudDBEngineDao cloudDBEngineDao;

    public void createTable(MetaTableDto table, UserDto userDto) {
        String sql = codeGenerator.createTableSql(table, userDto);
        cloudDBEngineDao.createTable(sql);
        table.setDateCreation(LocalDate.now().toString());

        MetaColumnDto columnDto = new MetaColumnDto();
        columnDto.setName("RowNumber");
        columnDto.setDataType(DataTypeEnum.INT);
        columnDto.setSizeConstraint(10);
        table.getColumns().add(0, columnDto);
        metaTableColumnDao.createTable(table);
    }

    public List<MetaTableDto> getTablesList(int dbId) {
        return metaTableColumnDao.getTablesList(dbId);
    }

    public MetaTableDto getFullMetaTableDto(int tableId) {


        return metaTableColumnDao.getFullMetaTableDto(tableId);
    }
}
