package com.den.cloud.services;

import com.den.cloud.dao.UserDatabaseDao;
import com.den.cloud.dto.UserDatabaseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Denys Vysochin on 29.11.2016.
 */

@Service
public class UserDatabaseService {

    @Autowired
    private UserDatabaseDao userDatabaseDao;

    public List<UserDatabaseDto> getUserDatabaseList(int userId) {
        return userDatabaseDao.getUserDatabaseList(userId);
    }

    public void createUserDatabase(UserDatabaseDto userDatabaseDto) {
        userDatabaseDto.setDateCreation(LocalDate.now().toString());
        userDatabaseDao.createUserDatabase(userDatabaseDto);
    }

    public UserDatabaseDto getUserDatabase(int id) {
        return userDatabaseDao.getUserDatabase(id);
    }

}
