package com.den.cloud.controllers.app.api;

import com.den.cloud.dto.UserDatabaseDto;
import com.den.cloud.dto.UserDto;
import com.den.cloud.services.UserDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by Denys Vysochin on 28.11.2016.
 */

@RestController
@RequestMapping("/user/db")
public class UserDatabaseController {

    @Autowired
    private UserDatabaseService userDatabaseService;

    @RequestMapping(value = "/create",  method = RequestMethod.POST)
    public String createDb(HttpSession session, @ModelAttribute("newDb") UserDatabaseDto userDatabase) throws IOException {
        try {
            userDatabase.setUserId(((UserDto) session.getAttribute("AUTH_USER")).getId());
            userDatabaseService.createUserDatabase(userDatabase);
            return "/user/dbList";
        } catch (Exception e) {
            return "{\"errorMessage\":\""+e.getMessage()+"\"}";
        }

    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<UserDatabaseDto> getUserDatabaseList(HttpSession session) {
        return userDatabaseService.getUserDatabaseList(((UserDto) session.getAttribute("AUTH_USER")).getId());
    }

    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public UserDatabaseDto getInfo(@PathVariable("id") int id) {
        return userDatabaseService.getUserDatabase(id);
    }

}
