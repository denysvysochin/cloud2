package com.den.cloud.controllers.app.api;

import com.den.cloud.dto.UserDto;
import com.den.cloud.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by Denys Vysochin on 04.12.2016.
 */

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user/profile/info", method = RequestMethod.GET)
    public UserDto getUser(HttpSession session) {
        return userService.getUser(((UserDto)session.getAttribute("AUTH_USER")).getId());
    }
}
