package com.den.cloud.controllers.app.api;

import com.den.cloud.dto.UserDto;
import com.den.cloud.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denys Vysochin on 03.12.2016.
 */

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registrate", method = RequestMethod.POST)
    public String registrate(@ModelAttribute("user") UserDto user) {
        userService.createUser(user);
        return "/registration/successRegistration";
    }


    @RequestMapping(value = "/validateEmail", method = RequestMethod.GET)
    public String validateEmail(@ModelAttribute("email")String email) {
        return !userService.emailIsExist(email) + "";
    }
}
