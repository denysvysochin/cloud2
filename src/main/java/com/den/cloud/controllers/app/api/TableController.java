package com.den.cloud.controllers.app.api;

import com.den.cloud.dto.MetaTableDto;
import com.den.cloud.dto.UserDto;
import com.den.cloud.services.MetaTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */

@RestController
@RequestMapping("/user/table")
public class TableController {

    @Autowired
    private MetaTableService metaTableService;



    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createTable(HttpSession session, @RequestBody final MetaTableDto table) {
        metaTableService.createTable(table, (UserDto) session.getAttribute("AUTH_USER"));
        return "/user/dbList";
    }

    @RequestMapping(value = "/list/{dbId}", method = RequestMethod.GET)
    public List<MetaTableDto> getTablesList(@PathVariable("dbId") int dbId) {
        return metaTableService.getTablesList(dbId);
    }

    @RequestMapping(value = "/{tableId}")
    public MetaTableDto getTableInfo(@PathVariable("tableId") int tableId) {
        return metaTableService.getFullMetaTableDto(tableId);
    }
}
