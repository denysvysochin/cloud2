package com.den.cloud.controllers;

import com.den.cloud.dao.UserDao;
import com.den.cloud.dao.UserDatabaseDao;
import com.den.cloud.dao.VisitsDao;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;

/**
 * Created by Denys Vysochin on 28.11.2016.
 */

@Controller
public class PageController {

    @Autowired
    private VisitsDao visitsDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserDatabaseDao userDatabaseDao;

    @RequestMapping("/")
    public String getHome() {
        visitsDao.addVisit();
        return "home";
    }

    @RequestMapping(value = "/user/dbList", method = RequestMethod.GET)
    public String getDbList() {
        return "dbList";
    }

    @RequestMapping(value = "/user/createDb", method = RequestMethod.GET)
    public String getCreateDb() {
        return "createDb";
    }

    @RequestMapping(value = "/user/userDbInfo/{dbId}", method = RequestMethod.GET)
    public String getDbInfo(@PathVariable("dbId") int id, ModelMap map) {
        map.addAttribute("dbId", id);
        return "userDbInfo";
    }

    @RequestMapping(value = "/user/createTable/{dbId}", method = RequestMethod.GET)
    public String getCreateTable(@PathVariable("dbId") int id, ModelMap map) {
        map.addAttribute("dbId", id);
        return "createTable";
    }

    @RequestMapping(value = "/user/tableInfo/{tableId}", method = RequestMethod.GET)
    public String getTableInfo(@PathVariable("tableId") int id, ModelMap map) {
        map.addAttribute("tableId", id);
        return "tableInfo";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String getRegistration() {
        return "registration";
    }

    @RequestMapping(value = "/registration/successRegistration", method = RequestMethod.GET)
    public String getSuccess() {
        return "successRegistration";
    }

    @RequestMapping(value = "/documentation", method = RequestMethod.GET)
    public String getDocumentation() {
        return "documentation";
    }

    @RequestMapping(value = "/user/support", method = RequestMethod.GET)
    public String getSupport() {
        return "support";
    }

    @RequestMapping(value = "/user/profile", method = RequestMethod.GET)
    public String getProfile() {
        return "profile";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String getAdmin(ModelMap map) {
        File file = new File("c:");
        map.addAttribute("usable", (file.getTotalSpace() - file.getUsableSpace())/1024 /1024); ///unallocated / free disk space in bytes.
        map.addAttribute("free", file.getFreeSpace()/1024 /1024); ///unallocated / free disk space in bytes.
        map.addAttribute("visits", visitsDao.getVisits());
        map.addAttribute("usersCount", userDao.getUsersCount());
        map.addAttribute("orders", userDatabaseDao.getCount());
        map.addAttribute("users", userDatabaseDao.getUsers());


        return "admin";
    }

    @RequestMapping(value = "/order/{pack}")
    public String getOrder(@PathVariable("pack") Integer pack, ModelMap modelMap) {
        String extendedOrderId = "ttest" + System.currentTimeMillis();
        String premiumOrderId = "ttest" + System.currentTimeMillis();

        modelMap.addAttribute("extendedSignature", DigestUtils.sha1Hex("test|999|USD|1397120|Cloud DB. Package EXTENDED|" + extendedOrderId));
        modelMap.addAttribute("premiumSignature", DigestUtils.sha1Hex("test|2999|USD|1397120|Cloud DB. Package PREMIUM|" + premiumOrderId));
        modelMap.addAttribute("extendedOrderId", extendedOrderId);
        modelMap.addAttribute("premiumOrderId", premiumOrderId);
        modelMap.addAttribute("pack", pack);
        return "order";
    }

    @RequestMapping(value = "/pay", method = RequestMethod.GET)
    public ModelAndView method() {
        return new ModelAndView("redirect:" + "https://api.fondy.eu/api/checkout/redirect/");

    }
}
