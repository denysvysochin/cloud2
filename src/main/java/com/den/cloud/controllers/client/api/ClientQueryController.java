package com.den.cloud.controllers.client.api;

import com.den.cloud.bean.JsonBean;
import com.den.cloud.bean.QueryBean;
import com.den.cloud.cloudengine.EngineManager;
import com.den.cloud.dao.UserDao;
import com.den.cloud.dto.UserDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by Denys Vysochin on 03.12.2016.
 */

@RestController
public class ClientQueryController {

    @Autowired
    private EngineManager engineManager;
    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/query/run/{dbName}/{userKey}", method = RequestMethod.GET)
    public String runQuery(@ModelAttribute QueryBean queryBean, @PathVariable("dbName") String dbName,
                           @PathVariable("userKey") String userKey,
                           HttpSession session) {
        try {
            return engineManager.runQuery(queryBean.getQuery(), dbName, userKey, userDao.getUserByKey(userKey));
        } catch (Exception ex) {
            return "{\"errorMessage\":\"Please, fill correct data!\"}";
        }
    }

    @RequestMapping(value = "/json/create/{dbName}/{tableName}/{userKey}", method = RequestMethod.GET)
    public String runJsonCreate(@ModelAttribute JsonBean jsonBean, @PathVariable("dbName") String dbName,
                                @PathVariable("tableName") String tableName,
                                @PathVariable("userKey") String userKey,
                                    HttpSession session) {
        try {
            return engineManager.processJsonCreate(jsonBean.getJson(), dbName, tableName, userKey, userDao.getUserByKey(userKey));
        } catch (Exception ex) {
            return "{\"errorMessage\":\"Incorrect request!\"}";
        }
    }
    @RequestMapping(value = "/json/update/{dbName}/{tableName}/{userKey}", method = RequestMethod.GET)
    public String runJsonUpdate(@ModelAttribute JsonBean jsonBean, @PathVariable("dbName") String dbName,
                                    @PathVariable("tableName") String tableName,
                                    @PathVariable("userKey") String userKey,
                                    HttpSession session) {
        try {
            return engineManager.processJsonUpdate(jsonBean.getJson(), dbName, tableName, userKey, userDao.getUserByKey(userKey));
        } catch (Exception ex) {
            return "{\"errorMessage\":\"Incorrect request!\"}";
        }
    }

    @RequestMapping(value = "/json/delete/{dbName}/{tableName}/{userKey}/{rowNumber}", method = RequestMethod.GET)
    public String runJsonDelete(@ModelAttribute JsonBean jsonBean, @PathVariable("dbName") String dbName,
                                    @PathVariable("tableName") String tableName,
                                    @PathVariable("userKey") String userKey,
                                    @PathVariable("rowNumber") String rowNumber,
                                    HttpSession session) {
        try {
            return engineManager.processJsonDelete(dbName, tableName, Integer.parseInt(rowNumber), userKey, userDao.getUserByKey(userKey));
        } catch (Exception ex) {
            return "{\"errorMessage\":\"Incorrect request!\"}";
        }
    }

    @RequestMapping(value = "/json/getAll/{dbName}/{tableName}/{userKey}", method = RequestMethod.GET)
    public String runJsonGetAll(@ModelAttribute JsonBean jsonBean, @PathVariable("dbName") String dbName,
                                    @PathVariable("tableName") String tableName,
                                    @PathVariable("userKey") String userKey) {
        try {
            return engineManager.processJsonGetAll(dbName, tableName, userKey, userDao.getUserByKey(userKey));
        } catch (Exception ex) {
            return "{\"errorMessage\":\"Incorrect request!\"}";
        }
    }
}
