package com.den.cloud.cloudengine;

import com.den.cloud.dao.CloudDBEngineDao;
import com.den.cloud.dao.MetaTableColumnDao;
import com.den.cloud.dao.UserDatabaseDao;
import com.den.cloud.dto.MetaTableDto;
import com.den.cloud.dto.UserDto;
import com.den.cloud.entities.User;
import com.den.cloud.entities.UserDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Denys Vysochin on 28.11.2016.
 */


@Component
@Transactional
public class EngineManager {

    @Autowired
    private CloudDBEngineDao cloudDBEngineDao;

    @Autowired
    private ValidationEngine validationEngine;

    @Autowired
    private UserDatabaseDao userDatabaseDao;

    @Autowired
    private CodeGenerator codeGenerator;

    @Autowired
    private MetaTableColumnDao metaTableColumnDao;
    @Transactional
    public String runQuery(String query, String dbName, String userKey, UserDto user) {

        UserDatabase userDatabase = userDatabaseDao.getUserDatabaseEntity(user.getId(), dbName);
        User userEntity = userDatabase.getUser();

        if (!userEntity.getUserKey().equals(userKey)) {
            return "{errorMessage:'Not permitted query'}";
        }
        if (validationEngine.validateSqlQuery(query, userDatabase)) {

            String jsonResult = "";
            List resultList = cloudDBEngineDao.runQuery(codeGenerator.processSql(query, userDatabase), userDatabase);
            Gson gson = new Gson();
            jsonResult = gson.toJson(resultList);

            return jsonResult;
        } else {
            return "{errorMessage:'Not permitted query'}";
        }

    }

    public String processJson(String json, String action, String dbName, String userKey, UserDto user) {
        return "";
    }

    public String processJsonUpdate(String json, String dbName, String tableName, String userKey, UserDto user) {
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> myMap = gson.fromJson(json, type);
        String sql = "UPDATE " + tableName + " SET  ";
        Integer rowNumber = 0;

        for (Map.Entry<String, String> entry : myMap.entrySet()) {
            if (entry.getKey().equals("RowNumber")) {
                rowNumber = Integer.parseInt(entry.getValue());
            } else {
                sql += entry.getKey() + " = '" + entry.getValue() + "',";
            }
        }
        if (rowNumber == 0) {
            throw new RuntimeException("RowNumber is not present");
        }
        sql = sql.substring(0, sql.length()-1) + " WHERE RowNumber = " + rowNumber + ";";

        UserDatabase userDatabase = userDatabaseDao.getUserDatabaseEntity(user.getId(), dbName);
        cloudDBEngineDao.runQuery(codeGenerator.processSql(sql, userDatabase), userDatabase);

        return "{\"status\":\"ok\"}";
    }

    public String processJsonCreate(String json, String dbName, String tableName, String userKey, UserDto user) {
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> myMap = gson.fromJson(json, type);
        String sql = "INSERT INTO " + tableName + " (";

        for (Map.Entry<String, String> entry : myMap.entrySet()) {
            sql += entry.getKey() + ",";
            //System.out.println(entry.getKey() + "/" + entry.getValue());
        }
        sql = sql.substring(0, sql.length()-1) + ") ";

        sql += " VALUES (";
        for (Map.Entry<String, String> entry : myMap.entrySet()) {
            sql += "'" + entry.getValue() + "',";
            //System.out.println(entry.getKey() + "/" + entry.getValue());
        }
        sql = sql.substring(0, sql.length()-1) + "); ";

        UserDatabase userDatabase = userDatabaseDao.getUserDatabaseEntity(user.getId(), dbName);
        cloudDBEngineDao.runQuery(codeGenerator.processSql(sql, userDatabase), userDatabase);

        return "{\"status\":\"ok\"}";
    }

    public String processJsonGetAll(String dbName, String tableName, String userKey, UserDto user) {
        Gson gson = new Gson();
        List<JsonObject> result = new ArrayList<>();
        UserDatabase userDatabase = userDatabaseDao.getUserDatabaseEntity(user.getId(), dbName);
        if (validationEngine.validateTablesPermission(" " + tableName, userDatabase)) {
            List<Object[]> resultList = cloudDBEngineDao.runQuery(codeGenerator.processSql("select * from " + tableName + ";", userDatabase), userDatabase);
            MetaTableDto metaTableDto = metaTableColumnDao.getFullMetaTableDtoByName(tableName, userDatabase.getId());
            for (Object[] arr: resultList) {
                JsonObject jsonObject = new JsonObject();
                for (int i=0; i<arr.length; i++) {
                    jsonObject.addProperty(metaTableDto.getColumns().get(i).getName(), arr[i].toString());
                }
                result.add(jsonObject);
            }
        } else {
            return "{errorMessage:'Not permitted query'}";
        }

        return result.toString();
    }

    public String processJsonDelete(String dbName, String tableName, Integer rowNumber, String userKey, UserDto user) {
        UserDatabase userDatabase = userDatabaseDao.getUserDatabaseEntity(user.getId(), dbName);
        if (validationEngine.validateTablesPermission(" " + tableName, userDatabase)) {
            cloudDBEngineDao.runQuery(codeGenerator.processSql("delete from " + tableName + " where RowNumber = " + rowNumber + ";", userDatabase), userDatabase);
        } else {
            return "{errorMessage:'Not permitted query'}";
        }

        return "{\"status\":\"ok\"}";
    }
}
