package com.den.cloud.cloudengine;

import com.den.cloud.dao.MetaTableColumnDao;
import com.den.cloud.entities.MetaTable;
import com.den.cloud.entities.UserDatabase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Denys Vysochin on 03.12.2016.
 */

@Service
@Transactional
public class ValidationEngine {

    @Autowired
    private MetaTableColumnDao metaTableColumnDao;

    @Transactional
    public boolean validateSqlQuery(String sql, UserDatabase database) {
        return validateTablesPermission(sql, database) && validateOperations(sql);
    }

    @Transactional
    public boolean validateTablesPermission(String sql, UserDatabase database) {
        List<MetaTable> userTables = database.getTables();
        List<MetaTable> allTables = metaTableColumnDao.getAllTables();

        List<MetaTable> notPermittedTables = allTables.stream()
                .filter(a -> !userTables.contains(a)).collect(Collectors.toList());

        for (MetaTable metaTable : notPermittedTables) {
            if (StringUtils.containsIgnoreCase(sql, " " + metaTable.getName() + ".") ||
                    StringUtils.containsIgnoreCase(sql, " " + metaTable.getName()) ||
                    StringUtils.containsIgnoreCase(sql, " " + metaTable.getName())) {
                return false;
            }
        }

        return true;
    }

    @Transactional
    public boolean validateOperations(String sql) {
        return StringUtils.containsIgnoreCase(sql, "SELECT ") || StringUtils.containsIgnoreCase(sql, "INSERT ")
                || StringUtils.containsIgnoreCase(sql, "UPDATE ") || StringUtils.containsIgnoreCase(sql, "DELETE ");
    }
}
