package com.den.cloud.cloudengine;

/**
 * Created by Denys Vysochin on 28.11.2016.
 */

import com.den.cloud.dto.MetaColumnDto;
import com.den.cloud.dto.MetaTableDto;
import com.den.cloud.dto.UserDto;
import com.den.cloud.entities.MetaTable;
import com.den.cloud.entities.UserDatabase;
import com.den.cloud.enums.DataTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
@Transactional
public class CodeGenerator {

    public String createTableSql(MetaTableDto metaTableDto, UserDto userDto) {
        String tableCreationQuery = "CREATE TABLE "+metaTableDto.getName() + userDto.getUserKey() + " ( ";
        tableCreationQuery += "RowNumber INT AUTO_INCREMENT UNIQUE, ";
        for(MetaColumnDto column: metaTableDto.getColumns()) {
            tableCreationQuery += column.getName() + " ";
            if (column.getDataType().equals(DataTypeEnum.TEXT)) {
                tableCreationQuery += column.getDataType().name() + " ";
            } else {
                tableCreationQuery += column.getDataType().name() + "(" + column.getSizeConstraint() +") ";
            }

            if(column.isNotNullConstraint()) {
                tableCreationQuery += "NOT NULL ";
            }
            if(column.isUniqueConstraint()) {
                tableCreationQuery += "UNIQUE ";
            }
            tableCreationQuery += ", ";
        }

        tableCreationQuery = tableCreationQuery.substring(0, tableCreationQuery.length()-2);
        tableCreationQuery += " )";

        return tableCreationQuery;
    }

    @Transactional
    public String processSql(String sql, UserDatabase userDatabase) {

        List<MetaTable> tables = userDatabase.getTables();
        tables.sort((a,b)->{
            if (a.getName().length() > b.getName().length())
                return -1;
            else if (a.getName().length() < b.getName().length()) {
                return 1;
            } else {
                return 0;
            }
        });
        for (MetaTable table : tables) {
            if (StringUtils.containsIgnoreCase(sql, table.getName()+".")) {
                sql = sql.replace(table.getName()+".", table.getName()+userDatabase.getUser().getUserKey()+".");
                break;
            } else if (StringUtils.containsIgnoreCase(sql, " "+table.getName()+" ")) {
                sql = sql.replace(table.getName(), " "+table.getName()+userDatabase.getUser().getUserKey()+" ");
                break;
            }  else if (StringUtils.containsIgnoreCase(sql, " "+table.getName())) {
                sql = sql.replace(" " + table.getName(), " "+table.getName()+userDatabase.getUser().getUserKey());
                break;
            }
        }

        return sql;
    }
}
