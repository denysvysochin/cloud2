package com.den.cloud.bean;

/**
 * Created by Denys Vysochin on 03.12.2016.
 */
public class EmailBean {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
