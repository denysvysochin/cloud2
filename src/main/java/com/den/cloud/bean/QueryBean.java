package com.den.cloud.bean;

/**
 * Created by Denys Vysochin on 03.12.2016.
 */
public class QueryBean {

    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
