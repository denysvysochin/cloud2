package com.den.cloud.bean;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Denys Vysochin on 01.12.2016.
 */
public class UserBean extends org.springframework.security.core.userdetails.User{
        private Integer id;

        public UserBean(Integer id, String username, String password, Collection<? extends GrantedAuthority> authorities) {
            super(username, password, authorities);
            this.id = id;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
}