package com.den.cloud.dto;

import com.den.cloud.enums.DataTypeEnum;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
public class MetaColumnDto {

    private int id;

    private String name;

    private DataTypeEnum dataType;

    private int sizeConstraint;

    private boolean notNullConstraint;

    private boolean uniqueConstraint;

    private int table_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataTypeEnum getDataType() {
        return dataType;
    }

    public void setDataType(DataTypeEnum dataType) {
        this.dataType = dataType;
    }

    public int getSizeConstraint() {
        return sizeConstraint;
    }

    public void setSizeConstraint(int sizeConstraint) {
        this.sizeConstraint = sizeConstraint;
    }

    public boolean isNotNullConstraint() {
        return notNullConstraint;
    }

    public void setNotNullConstraint(boolean notNullConstraint) {
        this.notNullConstraint = notNullConstraint;
    }

    public boolean isUniqueConstraint() {
        return uniqueConstraint;
    }

    public void setUniqueConstraint(boolean uniqueConstraint) {
        this.uniqueConstraint = uniqueConstraint;
    }

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }
}
