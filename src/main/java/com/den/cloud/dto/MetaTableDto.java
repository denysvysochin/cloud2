package com.den.cloud.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
public class MetaTableDto {

    private int id;

    private String name;

    private String dateCreation;

    private List<MetaColumnDto> columns = new ArrayList<>();

    private int dbId;

    private String dbName;

    private String userKey;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<MetaColumnDto> getColumns() {
        return columns;
    }

    public void setColumns(List<MetaColumnDto> columns) {
        this.columns = columns;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }
}
