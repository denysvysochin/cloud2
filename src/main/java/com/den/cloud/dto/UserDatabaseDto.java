package com.den.cloud.dto;

import com.den.cloud.entities.User;
import com.den.cloud.enums.DbTypeEnum;

/**
 * Created by Denys Vysochin on 28.11.2016.
 */
public class UserDatabaseDto {

    private int id;

    private String dbName;

    private DbTypeEnum type;

    private User user;

    private int userId;

    private String dateCreation;

    private Double spaceLeft;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public DbTypeEnum getType() {
        return type;
    }

    public void setType(DbTypeEnum type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Double getSpaceLeft() {
        return spaceLeft;
    }

    public void setSpaceLeft(Double spaceLeft) {
        this.spaceLeft = spaceLeft;
    }
}
