package com.den.cloud.components;

import com.den.cloud.dao.VisitsDao;
import com.den.cloud.dto.UserDto;
import com.den.cloud.mapping.UserMapping;
import com.den.cloud.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Denys Vysochin on 01.12.2016.
 */

@Component
public class AuthenticationSuccessFilter extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private UserService userService;

    @Autowired
    private VisitsDao visitsDao;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {

        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        com.den.cloud.entities.User user = userService.getUserEntityByEmail(authUser.getUsername());
        UserDto userDto = UserMapping.toDto(user);
        request.getSession().setAttribute("AUTH_USER", userDto);
        visitsDao.addVisit();

        super.onAuthenticationSuccess(request, response, authentication);
    }

}
