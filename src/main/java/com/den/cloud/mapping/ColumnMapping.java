package com.den.cloud.mapping;

import com.den.cloud.dto.MetaColumnDto;
import com.den.cloud.entities.MetaColumn;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
public class ColumnMapping {

    public static MetaColumnDto toDto(MetaColumn metaColumn) {
        MetaColumnDto metaColumnDto = new MetaColumnDto();

        metaColumnDto.setId(metaColumn.getId());
        metaColumnDto.setName(metaColumn.getName());
        metaColumnDto.setDataType(metaColumn.getDataType());
        metaColumnDto.setNotNullConstraint(metaColumn.isNotNullConstraint());
        metaColumnDto.setSizeConstraint(metaColumn.getSizeConstraint());
        metaColumnDto.setUniqueConstraint(metaColumn.isUniqueConstraint());

        return  metaColumnDto;
    }


    public static MetaColumn createEntity(MetaColumnDto metaColumnDto) {
        MetaColumn metaColumn = new MetaColumn();

        metaColumn.setName(metaColumnDto.getName());
        metaColumn.setDataType(metaColumnDto.getDataType());
        metaColumn.setNotNullConstraint(metaColumnDto.isNotNullConstraint());
        metaColumn.setSizeConstraint(metaColumnDto.getSizeConstraint());
        metaColumn.setUniqueConstraint(metaColumnDto.isUniqueConstraint());

        return metaColumn;
    }
}
