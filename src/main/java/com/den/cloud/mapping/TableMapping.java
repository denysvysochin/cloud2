package com.den.cloud.mapping;

import com.den.cloud.dto.MetaTableDto;
import com.den.cloud.entities.MetaTable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
@Transactional
public class TableMapping {

    public static MetaTableDto toDto(MetaTable metaTable) {
        MetaTableDto metaTableDto = new MetaTableDto();

        metaTableDto.setId(metaTable.getId());
        metaTableDto.setName(metaTable.getName());
        metaTableDto.setDateCreation(metaTable.getDateCreation());
        metaTableDto.setDbName(metaTable.getUserDatabase().getDbName());
        metaTableDto.setUserKey(metaTable.getUserDatabase().getUser().getUserKey());

        return metaTableDto;
    }

    public static MetaTable createEntity(MetaTableDto metaTableDto) {
        MetaTable metaTable = new MetaTable();

        metaTable.setName(metaTableDto.getName());
        metaTable.setDateCreation(metaTableDto.getDateCreation());

        return metaTable;
    }
}
