package com.den.cloud.mapping;

import com.den.cloud.dto.UserDatabaseDto;
import com.den.cloud.entities.UserDatabase;

/**
 * Created by Denys Vysochin on 29.11.2016.
 */
public class UserDatabaseMapper {

    public static UserDatabaseDto toDto(UserDatabase userDatabase) {
        UserDatabaseDto userDatabaseDto = new UserDatabaseDto();

        userDatabaseDto.setDbName(userDatabase.getDbName());
        userDatabaseDto.setId(userDatabase.getId());
        userDatabaseDto.setType(userDatabase.getType());
        userDatabaseDto.setDateCreation(userDatabase.getDateCreation());
        userDatabaseDto.setSpaceLeft(userDatabase.getSpaceLeft());
        //userDatabaseDto.setUser();
        return userDatabaseDto;
    }

}
