package com.den.cloud.mapping;

import com.den.cloud.dto.UserDto;
import com.den.cloud.entities.User;

/**
 * Created by Denys Vysochin on 01.12.2016.
 */
public class UserMapping {

    public static UserDto toDto(User user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setSecondName(user.getSecondName());
        userDto.setUserKey(user.getUserKey());

        return userDto;
    }

    public static User createEntity(UserDto userDto) {
        User user = new User();

        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setSecondName(userDto.getSecondName());
        user.setUserRole(userDto.getUserRole());
        user.setUserKey(userDto.getUserKey());

        return user;
    }
}
