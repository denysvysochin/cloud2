package com.den.cloud.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
@Entity
@Table(name = "meta_table")
public class MetaTable {

    @Id
    @Column(name = "table_id")
    @GeneratedValue//(strategy= GenerationType.SEQUENCE, generator = "meta_table_ids")
    //@SequenceGenerator(name="meta_table_ids", sequenceName="meta_table_ids",allocationSize=1)
    private int id;

    @Column(name = "table_name")
    private String name;

    @Column(name = "date_creation")
    private String dateCreation;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "metaTable")
    private List<MetaColumn> columns = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "db_id")
    private UserDatabase userDatabase;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<MetaColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<MetaColumn> columns) {
        this.columns = columns;
    }

    public UserDatabase getUserDatabase() {
        return userDatabase;
    }

    public void setUserDatabase(UserDatabase userDatabase) {
        this.userDatabase = userDatabase;
    }

    @Override
    public boolean equals(Object obj) {
        return this.name == ((MetaTable) obj).getName();
    }
}
