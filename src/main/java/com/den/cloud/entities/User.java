package com.den.cloud.entities;

import com.den.cloud.enums.UserRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "USER_TABLE")
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue//(strategy= GenerationType.SEQUENCE, generator = "user_ids")
    //@SequenceGenerator(name="user_ids", sequenceName="user_ids",allocationSize=1)
    private int id;

    @Column(name = "user_name")
    private String name;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private UserRole userRole;

    @Column(name = "user_key")
    private String userKey;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserDatabase> userDatabaseList = new ArrayList<>();

    @Transient
    private Integer testCount;
    @Transient
    private Integer extendetCount;
    @Transient
    private Integer premiumCount;

    public Integer getTestCount() {
        return testCount;
    }

    public void setTestCount(Integer testCount) {
        this.testCount = testCount;
    }

    public Integer getExtendetCount() {
        return extendetCount;
    }

    public void setExtendetCount(Integer extendetCount) {
        this.extendetCount = extendetCount;
    }

    public Integer getPremiumCount() {
        return premiumCount;
    }

    public void setPremiumCount(Integer premiumCount) {
        this.premiumCount = premiumCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public List<UserDatabase> getUserDatabaseList() {
        return userDatabaseList;
    }

    public void setUserDatabaseList(List<UserDatabase> userDatabaseList) {
        this.userDatabaseList = userDatabaseList;
    }
}
