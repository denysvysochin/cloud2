package com.den.cloud.entities;

import com.den.cloud.enums.DataTypeEnum;

import javax.persistence.*;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
@Entity
@Table(name = "META_COLUMN")
public class MetaColumn {

    @Id
    @Column(name = "column_id")
    @GeneratedValue//(strategy= GenerationType.SEQUENCE, generator = "meta_column_ids")
    //@SequenceGenerator(name="meta_column_ids", sequenceName="meta_column_ids",allocationSize=1)
    private int id;

    @Column(name = "column_name")
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "data_type")
    private DataTypeEnum dataType;

    @Column(name = "size_constraint")
    private int sizeConstraint;

    @Column(name = "not_null_constraint")
    private boolean notNullConstraint;

    @Column(name = "unique_constraint")
    private boolean uniqueConstraint;

    @ManyToOne
    @JoinColumn(name = "table_id")
    private MetaTable metaTable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataTypeEnum getDataType() {
        return dataType;
    }

    public void setDataType(DataTypeEnum dataType) {
        this.dataType = dataType;
    }

    public int getSizeConstraint() {
        return sizeConstraint;
    }

    public void setSizeConstraint(int sizeConstraint) {
        this.sizeConstraint = sizeConstraint;
    }

    public boolean isNotNullConstraint() {
        return notNullConstraint;
    }

    public void setNotNullConstraint(boolean notNullConstraint) {
        this.notNullConstraint = notNullConstraint;
    }

    public boolean isUniqueConstraint() {
        return uniqueConstraint;
    }

    public void setUniqueConstraint(boolean uniqueConstraint) {
        this.uniqueConstraint = uniqueConstraint;
    }

    public MetaTable getMetaTable() {
        return metaTable;
    }

    public void setMetaTable(MetaTable metaTable) {
        this.metaTable = metaTable;
    }
}
