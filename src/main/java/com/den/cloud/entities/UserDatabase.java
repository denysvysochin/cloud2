package com.den.cloud.entities;

import com.den.cloud.enums.DbTypeEnum;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denys Vysochin on 28.11.2016.
 */
@Entity
@Table(name = "USER_DB")
public class UserDatabase {

    @Id
    @Column(name = "db_id")
    @GeneratedValue//(strategy= GenerationType.SEQUENCE, generator = "user_db_ids")
    //@SequenceGenerator(name="user_db_ids", sequenceName="user_db_ids",allocationSize=1)
    private int id;

    @Column(name = "db_name")
    private String dbName;

    @Column(name = "db_type")
    @Enumerated(EnumType.STRING)
    private DbTypeEnum type;

    @Column(name = "date_creation")
    private String dateCreation;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userDatabase")
    private List<MetaTable> tables = new ArrayList<>();

    @Transient
    private Double spaceLeft;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public DbTypeEnum getType() {
        return type;
    }

    public void setType(DbTypeEnum type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<MetaTable> getTables() {
        return tables;
    }

    public void setTables(List<MetaTable> tables) {
        this.tables = tables;
    }

    public Double getSpaceLeft() {
        return spaceLeft;
    }

    public void setSpaceLeft(Double spaceLeft) {
        this.spaceLeft = spaceLeft;
    }

    //Maybe in future
    //private UserProject userProject;
}
