package com.den.cloud.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by admin-pc on 02.12.2017.
 */

@Entity
public class Visits {

    @Id
    @Column(name = "user_id")
    @GeneratedValue//(strategy= GenerationType.SEQUENCE, generator = "user_ids")
    //@SequenceGenerator(name="user_ids", sequenceName="user_ids",allocationSize=1)
    private int id;

    private Integer numbers;

    public Integer getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }
}
