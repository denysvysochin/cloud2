package com.den.cloud.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by admin-pc on 02.12.2017.
 */
@Repository
@Transactional
public class VisitsDao {

    @Autowired
    private SessionFactory sessionFactory;


    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void addVisit() {
        getSession().createSQLQuery("UPDATE VISITS SET numbers = numbers + 1;").executeUpdate();
    }

    public Integer getVisits() {
        return (Integer) getSession().createSQLQuery("SELECT numbers FROM VISITS WHERE id = 1;").list().get(0);
    }
}
