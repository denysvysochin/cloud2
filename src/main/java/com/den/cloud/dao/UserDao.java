package com.den.cloud.dao;

import com.den.cloud.dto.UserDto;
import com.den.cloud.entities.User;
import com.den.cloud.mapping.UserMapping;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Denys Vysochin on 29.11.2016.
 */

@Repository
@Transactional
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public User getUserEntity(int userId) {
        return (User) getSession().get(User.class, userId);
    }

    public UserDto getUser(int id) {
        return UserMapping.toDto(getUserEntity(id));
    }

    public void saveUser(User user) {
        getSession().saveOrUpdate(user);
    }

    public Integer getUsersCount() {
        return getSession().createCriteria(User.class).list().size();
    }

    public UserDto getUserByKey(String key) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("userKey", key));
        return UserMapping.toDto((User)criteria.uniqueResult());
    }

    public User getUserEntityByEmail(String email) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email));

        return (User) criteria.uniqueResult();
    }

    public void createUser(UserDto userDto) {
        saveUser(UserMapping.createEntity(userDto));
    }
}
