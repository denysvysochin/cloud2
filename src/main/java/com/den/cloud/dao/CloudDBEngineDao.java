package com.den.cloud.dao;

import com.den.cloud.entities.MetaTable;
import com.den.cloud.entities.UserDatabase;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Denys Vysochin on 03.12.2016.
 */

@Repository
@Transactional
public class CloudDBEngineDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private UserDatabaseDao userDatabaseDao;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public List runQuery(String query, UserDatabase userDatabase) {
        Query sqlQuery = getSession().createSQLQuery(query);
        if (StringUtils.containsIgnoreCase(query, "update") || StringUtils.containsIgnoreCase(query, "insert") || StringUtils.containsIgnoreCase(query, "delete")) {
                Double space = 0.0;
                for (MetaTable table : userDatabase.getTables()) {
                    space += userDatabaseDao.getTableSize(table.getName() + userDatabase.getUser().getUserKey(), "clouddb");
                }
            userDatabase.setSpaceLeft(space);
            if (userDatabase.getSpaceLeft() >= userDatabase.getType().getSize()) {
                throw new RuntimeException("No memory in database");
            }
            sqlQuery.executeUpdate();
            return new ArrayList();
        } else {
            List resultList = sqlQuery.list();
            return resultList;
        }
    }

    public void createTable(String query) {
        Query sqlQuery = getSession().createSQLQuery(query);
        sqlQuery.executeUpdate();
    }
}
