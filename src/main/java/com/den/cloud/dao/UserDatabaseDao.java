package com.den.cloud.dao;

import com.den.cloud.dto.UserDatabaseDto;
import com.den.cloud.entities.MetaTable;
import com.den.cloud.entities.User;
import com.den.cloud.entities.UserDatabase;
import com.den.cloud.enums.DbTypeEnum;
import com.den.cloud.mapping.UserDatabaseMapper;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Denys Vysochin on 29.11.2016.
 */

@Repository
@Transactional
public class UserDatabaseDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDao userDao;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void saveUserDatabase(UserDatabase userDatabase) {
        getSession().saveOrUpdate(userDatabase);
    }

    public Integer getCount() {
        return getSession().createCriteria(UserDatabase.class).list().size();
    }

    public List<User> getUsers() {
        return (List<User>) getSession().createCriteria(User.class).list().stream().map(u -> {
            User uu = (User) u;
            uu.setTestCount(uu.getUserDatabaseList().stream().filter(d -> d.getType().equals(DbTypeEnum.TEST)).collect(Collectors.toList()).size());
            uu.setExtendetCount(uu.getUserDatabaseList().stream().filter(d -> d.getType().equals(DbTypeEnum.EXTENDED)).collect(Collectors.toList()).size());
            uu.setPremiumCount(uu.getUserDatabaseList().stream().filter(d -> d.getType().equals(DbTypeEnum.PREMIUM)).collect(Collectors.toList()).size());
            return uu;
        }).collect(Collectors.toList());
    }

    public UserDatabase getUserDatabaseEntity(int id) {
        return (UserDatabase) getSession().get(UserDatabase.class, id);
    }
    public UserDatabase getUserDatabaseEntity(int id, String name) {
        Criteria criteria = getSession().createCriteria(UserDatabase.class);
        criteria.createAlias("user", "u");
        criteria.add(Restrictions.eq("u.id", id));
        criteria.add(Restrictions.eq("dbName", name));
        return (UserDatabase) criteria.uniqueResult();
    }


    public UserDatabaseDto getUserDatabase(int id) {
        return UserDatabaseMapper.toDto(getUserDatabaseEntity(id));
    }

    public List<UserDatabaseDto> getUserDatabaseList(int userId) {
        Criteria criteria = getSession().createCriteria(UserDatabase.class);
        criteria.createAlias("user", "u");
        criteria.add(Restrictions.eq("u.id", userId));
        List<UserDatabase> list = criteria.list();
        for (UserDatabase db: list) {
            Double space = 0.0;
            for (MetaTable table : db.getTables()) {
                space += getTableSize(table.getName() + db.getUser().getUserKey(), "clouddb");
            }
            db.setSpaceLeft(space);
        }
        List<UserDatabaseDto> resultList = list.stream().map(UserDatabaseMapper::toDto).collect(Collectors.toList());
        return resultList;
    }

    public void createUserDatabase(UserDatabaseDto userDatabaseDto) {
        UserDatabase userDatabase = new UserDatabase();
        User user = userDao.getUserEntity(userDatabaseDto.getUserId());
        List<UserDatabase> list = user.getUserDatabaseList();

        if (list.stream().anyMatch(db->db.getType().equals(DbTypeEnum.TEST)) && userDatabaseDto.getType().equals(DbTypeEnum.TEST)){
            throw new RuntimeException("You have already Test database");
        }

        userDatabase.setDbName(userDatabaseDto.getDbName());
        userDatabase.setType(userDatabaseDto.getType());
        userDatabase.setUser(user);
        userDatabase.setDateCreation(userDatabaseDto.getDateCreation());

        user.getUserDatabaseList().add(userDatabase);
        userDao.saveUser(user);
        saveUserDatabase(userDatabase);
    }

    public Double getTableSize(String tableName, String dataBaseName) {
        String query = "SELECT \n" +
                "    round(((data_length + index_length) / 1024 / 1024), 2) `Size in MB` \n" +
                "FROM information_schema.TABLES \n" +
                "WHERE table_schema = \"" + dataBaseName + "\" " +
                "    AND table_name = \"" + tableName + "\";";

        Query sqlQuery = getSession().createSQLQuery(query);
        List resultList = sqlQuery.list();
        if (resultList.isEmpty()) {
            return 0.0;
        }
        return Double.parseDouble((resultList.get(0)).toString());
    }

}
