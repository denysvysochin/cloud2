package com.den.cloud.dao;

import com.den.cloud.dto.MetaColumnDto;
import com.den.cloud.dto.MetaTableDto;
import com.den.cloud.entities.MetaColumn;
import com.den.cloud.entities.MetaTable;
import com.den.cloud.entities.UserDatabase;
import com.den.cloud.mapping.ColumnMapping;
import com.den.cloud.mapping.TableMapping;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */

@Repository
@Transactional
public class MetaTableColumnDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDatabaseDao userDatabaseDao;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void saveTable(MetaTable table) {
        getSession().saveOrUpdate(table);
    }

    public void saveColumn(MetaColumn column) {
        getSession().saveOrUpdate(column);
    }

    public MetaTable getMetaTableEntity(int id) {
        return (MetaTable) getSession().get(MetaTable.class, id);
    }

    public MetaTableDto getMetaTable(int id) {
        return TableMapping.toDto((MetaTable) getSession().get(MetaTable.class, id));
    }

    public List<MetaTable> getAllTables() {
        return getSession().createCriteria(MetaTable.class).list();
    }

    public List<MetaTableDto> getTablesList(int dbId) {
        Criteria criteria = getSession().createCriteria(MetaTable.class);
        criteria.createAlias("userDatabase", "db");
        criteria.add(Restrictions.eq("db.id", dbId));
        List<MetaTable> list = criteria.list();

        List<MetaTableDto> resultList;
        resultList = list.stream().map(TableMapping::toDto).collect(Collectors.toList());
        return resultList;
    }

    public MetaTableDto getFullMetaTableDto(int id) {
        MetaTable metaTable = (MetaTable) getSession().get(MetaTable.class, id);
        MetaTableDto metaTableDto = TableMapping.toDto(metaTable);
        metaTableDto.setDbId(metaTable.getUserDatabase().getId());
        for(MetaColumn metaColumn : metaTable.getColumns()) {
            metaTableDto.getColumns().add(ColumnMapping.toDto(metaColumn));
        }
        return metaTableDto;
    }

    public MetaTableDto getFullMetaTableDtoByName(String name, int dbId) {
        Criteria criteria = getSession().createCriteria(MetaTable.class);
        criteria.createAlias("userDatabase", "db");
        criteria.add(Restrictions.eq("db.id", dbId));
        criteria.add(Restrictions.eq("name", name));
        MetaTable metaTable = (MetaTable) criteria.list().get(0);
        MetaTableDto metaTableDto = TableMapping.toDto(metaTable);
        metaTableDto.setDbId(metaTable.getUserDatabase().getId());
        for(MetaColumn metaColumn : metaTable.getColumns()) {
            metaTableDto.getColumns().add(ColumnMapping.toDto(metaColumn));
        }
        return metaTableDto;
    }


    public void createTable(MetaTableDto tableDto) {
        MetaTable metaTable = TableMapping.createEntity(tableDto);

        UserDatabase userDatabase = userDatabaseDao.getUserDatabaseEntity(tableDto.getDbId());
        metaTable.setUserDatabase(userDatabase);
        userDatabase.getTables().add(metaTable);
        userDatabaseDao.saveUserDatabase(userDatabase);
        saveTable(metaTable);

        for(MetaColumnDto dto: tableDto.getColumns()) {
            MetaColumn metaColumn = ColumnMapping.createEntity(dto);
            metaColumn.setMetaTable(metaTable);

            saveColumn(metaColumn);
            metaTable.getColumns().add(metaColumn);
        }

        saveTable(metaTable);
    }

}
