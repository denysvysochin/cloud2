package com.den.cloud.enums;

/**
 * Created by Denys Vysochin on 29.11.2016.
 */
public enum DbTypeEnum {
    TEST(5), DEFAULT(1024), EXTENDED(100), PREMIUM(1024);

    private Integer size;
    DbTypeEnum(Integer size) {
        this.size = size;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
