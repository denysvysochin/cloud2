package com.den.cloud.enums;

/**
 * Created by Denys Vysochin on 30.11.2016.
 */
public enum  DataTypeEnum {

    INT, VARCHAR, TEXT, DECIMAL
}
