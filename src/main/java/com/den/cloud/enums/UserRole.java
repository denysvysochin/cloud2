package com.den.cloud.enums;

/**
 * Created by Denys Vysochin on 01.12.2016.
 */
public enum UserRole {

    ROLE_USER, ROLE_ADMIN
}
